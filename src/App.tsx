import "./App.css";
import { Select } from "./components/Select";

function App() {
  const stockRenderer = (stock: string) => {
    const nb = Number(stock);
    if (nb === 0) return "Epuisé";
    if (nb === 1) return "C'est le dernier !";
    if (nb < 5) return `Plus que ${nb} en stock !`;
    else return stock;
  };

  return (
    <div className="App">
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
        veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
        commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
        velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
        occaecat cupidatat non proident, sunt in culpa qui officia deserunt
        mollit anim id est laborum
      </p>
      <Select
        name="product-size"
        onSelect={(value) => {
          console.log(`selected ${value}`);
        }}
        stockRenderer={stockRenderer}
      >
        <option value="10" data-stock="1" data-price="18.00 €">
          S
        </option>
        <option value="11" data-stock="15" data-price="18.00 €">
          M
        </option>
        <option value="12" data-stock="0" data-price="18.00 €">
          L
        </option>
        <option value="13" data-stock="2" data-price="22.00 €">
          XL
        </option>
      </Select>
      <p>
        Sed ut perspiciatis unde omnis iste natus error sit voluptatem
        accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab
        illo inventore veritatis et quasi architecto beatae vitae dicta sunt
        explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut
        odit aut fugit, sed quia consequuntur magni dolores eos qui ratione
        voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum
        quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam
        eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat
        voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam
        corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?
        Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse
        quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo
        voluptas nulla pariatur?
      </p>
      <Select
        name="product-size"
        onSelect={(value) => {
          console.log(`selected ${value}`);
        }}
        stockRenderer={stockRenderer}
      >
        <option value="12" data-stock="0" data-price="18.00 €">
          L
        </option>
        <option value="13" data-stock="2" data-price="22.00 €">
          XL
        </option>
        <option value="14" data-stock="8" data-price="24.00 €">
          XXL
        </option>
        <option value="15" data-stock="2" data-price="26.00 €">
          XXXL
        </option>
      </Select>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
        veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
        commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
        velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
        occaecat cupidatat non proident, sunt in culpa qui officia deserunt
        mollit anim id est laborum
      </p>
    </div>
  );
}

export default App;
