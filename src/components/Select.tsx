import { useState } from "react";
import "./Select.scss";

type SelectProps = React.PropsWithChildren & {
  name: string;
  onSelect: (value: string) => void;
  stockRenderer?: (value: string) => string;
  label?: string; // option label, default value is "Veuillez sélectionner"
};

type OptionProps = {
  value: string;
  "data-stock": string;
  "data-price": string;
  children: string;
  stockRenderer?: (value: string) => string;
  onSelect: (value: string) => void;
};

const Option = (props: OptionProps) => (
  <div
    className="option"
    onClick={() => {
      props.onSelect(props.value);
    }}
  >
    <div className="size">{props.children}</div>
    <div className="qty">
      {props.stockRenderer
        ? props.stockRenderer(props["data-stock"])
        : props["data-stock"]}
    </div>
    <div className="price"> {props["data-price"]}</div>
  </div>
);

export const Select = (props: SelectProps) => {
  const [open, setOpen] = useState(false);
  const [selected, setSelected] = useState<string | null>(null);

  if (!Array.isArray(props.children))
    throw new Error("expected an array of options");

  const classes = ["selector"];

  // selection logic
  const onSelect = (value: string) => {
    setSelected(value);
    setOpen(false);
    props.onSelect(value);
  };
  let selectText = props.label || "Veuillez sélectionner";
  if (selected) {
    const selectedChild = props.children.find(
      (i) => i.props.value === selected
    );
    selectText = selectedChild.props.children;
  }

  // open/close logic
  const toggleOpen = () => setOpen(!open);
  if (open) classes.push("open");

  // component rendering
  return (
    <div className="select">
      <div onClick={toggleOpen} className={classes.join(" ")}>
        {selectText}
      </div>
      <div className="optionsContainer">
        {open ? (
          <div className="options">
            {props.children.map((i, idx) => (
              <Option
                key={idx}
                {...i.props}
                onSelect={onSelect}
                stockRenderer={props.stockRenderer}
              />
            ))}
          </div>
        ) : null}
      </div>
    </div>
  );
};
